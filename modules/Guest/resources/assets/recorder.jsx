import React  from 'react';
import ReactDom from 'react-dom';
import { ReactMic } from 'react-mic';

export class Recorder extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            record: false
        };

        this.startRecording = this.startRecording.bind(this);
        this.stopRecording = this.stopRecording.bind(this);
        this.onStop = this.onStop.bind(this);
        this.sendData = this.sendData.bind(this);
    }

    startRecording() {
        this.setState({
            record: true
        });
    }

    stopRecording() {
        this.setState({
            record: false
        });
    }

    sendData() {
        if (document.getElementById('sender').value === null) {
            swal("isian nama harus di isi");
        }

        var formData = new FormData();

        formData.append("record", this.state.recorded);
        formData.append("sender", document.getElementById('sender').value);
        formData.append("text_message", document.getElementById('text').value);
        var send = XMLHttpRequest.prototype.send;
        var request = new XMLHttpRequest();
        request.open("POST", url);
        request.setRequestHeader('X-CSRF-Token', document.head.querySelector('meta[name="csrf-token"]'));
        request.send(formData);

        document.getElementById('send').setAttribute('disabled', '');

        $("#form-message").fadeOut();
        $("#alert").fadeIn();

        swal("Pesan Terkirim");
    }

    onStop(recordedBlob) {
        this.setState({
            recorded: recordedBlob.blob
        });
    }

    render() {
        return (
            <div>
                <div id="form-message">
                    <div className="form-group">
                        <input type="text" className="form-control" id="sender" placeholder="Siapa yang mengirim pesan ?"/>
                    </div>
                    <div className="form-group">
                        <input type="text" className="form-control" id="text" placeholder="Masukkan pesan text"/>
                    </div>
                    <ReactMic
                        record={this.state.record} className="sound-wave"
                        onStop={this.onStop} strokeColor="#000000" backgroundColor="#1393b8" />
                    <button onClick={this.startRecording} type="button" className='btn btn-primary'>Start Rekam Suara</button>
                    <button onClick={this.stopRecording} type="button" className='btn btn-primary'>Stop Rekam Suara</button>
                    <button onClick={this.sendData} className="btn btn-info btn-block" id="send">Kirim pesan</button>
                </div>
                <div className="alert hidden" style={{ textAlign : 'center' }} id="alert">
                    <div className="alert alert-success">Pesan dikirim</div>
                </div>
            </div>
        );
    }
}

if (document.getElementById('recording-desk')) {
    ReactDom.render(<Recorder/>, document.getElementById('recording-desk'));
}