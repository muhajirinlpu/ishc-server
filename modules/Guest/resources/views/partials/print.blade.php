<style>
    html, body {
        color: #009b99;
        text-align: center;
    }
</style>
<div class="card">
    <header class="card-heading card-blue">
        <h2 class="card-title">Kode</h2>
    </header>
    <div class="card-body">
        <div class="text-center">
            <img src="data:image/png;base64, {!! $encode !!} ">
        </div>
        <h4>Scan kode diatas ini, buka link nya kemudian kirimkan pesan</h4>
    </div>
</div>
