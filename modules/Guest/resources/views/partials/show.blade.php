<div class="modal fade bottom-sheet" id="show-modal-{{ $item->id }}" tabindex="-1"
     role="dialog" aria-labelledby="show-modal-{{ $item->id }}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #FF1744">
                <h4 class="modal-title" >Pesan dari : {{ $item->sender }}</h4>
                <ul class="card-actions icons right-top">
                    <li>
                        <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
                            <i class="zmdi zmdi-close"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="modal-body scrollbar">
                <table class="table">
                    <tbody>
                        <tr>
                            <td>Pesan Text</td>
                            <td>{{ $item->text_message }}</td>
                        </tr>
                        <tr>
                            <td>Pesan Audio</td>
                            <td>
                                <audio controls style="max-width: 100%">
                                    <source src="{{ asset('storage'.substr($item->audio_message, 6, 100)) }}"
                                            type="audio/webm">
                                </audio>
                                <a target="_blank" href="{{ asset('storage'.substr($item->audio_message, 6, 100)) }}" class="link">
                                    klik disini jika tidak dapat di putar
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <form action="{{ route('app.guest.mark', [$item->device->hash, $item->id]) }}" method="post">
                    {{ csrf_field() }}
                    <input type="text" class="hidden" name="read" value="{{ $item->read == 0 ? 1 : 0 }}">
                    <button type="submit" class="btn btn-primary">{{ $item->read ? 'Tandai belum dibaca' : 'Tandai sudah dibaca' }}</button>
                </form>

                <form action="{{ route('app.guest.destroy', [$item->device->hash, $item->id]) }}" method="post" id="delete-form-{{ $item->id }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                </form>
                <button type="button" class="btn btn-warning swal-warn"
                        data-title="Yakin ingin menghapus pesan dari {{ $item->sender }}"
                        data-subtitle="Data tidak akan bisa kembali :("
                        data-target="#delete-form-{{ $item->id }}">Hapus</button>
            </div>
        </div>
    </div>
</div>