@extends('app.container')

@section('content')
    @each('guest::partials.show', $inbox, 'item')
    <div class="row">
        <form action="{{ route('app.guest.code-generator', ['device' => $device->hash]) }}"
              method="post" id="request-new-code">
            {{ csrf_field() }}
        </form>
        <form action="{{ route('app.guest.update', ['device' => $device->hash]) }}"
              method="post" id="status_form">
            {{ csrf_field() }}
            <div class="col-lg-4">
                <div class="card">
                    <header class="card-heading card-purple">
                        <h2 class="card-title">Status</h2>
                    </header>
                    <div class="card-body">
                        <div class="togglebutton m-b-15 btn-big">
                            <label>
                                <input type="checkbox" class="toggle-primary"
                                       @if(!empty($guest) && $guest->value->first() == true) checked @endif name="status">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <header class="card-heading card-blue">
                        <h2 class="card-title">Kode</h2>
                    </header>
                    <div class="card-body">
                        @isset($code)
                            <div class="text-center">
                                {!! QrCode::size(250)->generate(route('message.show', [$code->code])); !!}
                            </div>
                            <a href="{{ route('app.guest.print', [$device->hash]) }}" class="btn btn-info" target="_blank">Print</a>
                        @endisset
                        <button class="btn btn-primary" type="button"
                                onclick="document.getElementById('request-new-code').submit()">Request Kode Baru
                        </button>
                            <br>
                        <small>*Direkomendasikan mengganti kode secara berkala</small>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <button type="reset" class="btn btn-primary btn-flat ">Reset</button>
                        <button type="submit" class="btn btn-primary">Apply</button>
                    </div>
                </div>
            </div>
        </form>
        <div class="col-md-12">
            <div class="card">
                <header class="card-heading card-blue">
                    <h2 class="card-title">Pesan</h2>
                </header>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Pengirim</th>
                            <th>pada</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($inbox as $index => $item)
                            <tr class="{{ $item->read == 0 ? 'warning' : '' }}">
                                <td>{{ 1 + $index }}</td>
                                <td>{{ $item->sender }}</td>
                                <td>{{ $item->created_at->diffForHumans()  }}</td>
                                <td>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#show-modal-{{ $item->id }}">
                                        <i class="zmdi zmdi-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" style="text-align: center">Tidak ada pesan</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('stylesheet')
    <style>
        .btn-big {
            text-align: center;
            margin-top: 40px;
        }
        .btn-big > * > span {
            transform: scale(2);
        }
    </style>
@endpush

@push('javascript')
    <script>
        $('.swal-warn').bind('click', function(e) {
            e.preventDefault();
            const target = $(this).data('target');
            const title = $(this).data('title');
            const subtitle = $(this).data('subtitle');
            swal({
                title   : title,
                text    : subtitle,
                type    : "warning",
                showCancelButton    : true,
                confirmButtonColor  : "#DD6B55",
                confirmButtonText   : "Oke"
            }).then(function() {
                $(target).submit();
                console.log(target)
            }, function (dismiss) {
                if (dismiss === 'cancel') {
                    swal(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                }
            })
        });
    </script>
@endpush