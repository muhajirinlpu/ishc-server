<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="theme-color" content="#1976d2">
    @isset($pageMeta)
        @foreach($pageMeta as $name => $content)
            <meta name="{{ $name }}" content="{{ $content }}">
        @endforeach
    @endisset
    <title>{{ config('app.name') }} &ndash; {{ $pageTitle or 'Untitled' }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('partials.assets.stylesheet')
</head>
<body>
    <div class="container" style="margin-top: 100px;">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div id="recording-desk"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.assets.javascript')
    <script>var url = "{{ route('message.store', [$code->code]) }}"</script>
    <script src="{{ asset('js/components/recorder.js') }}"></script>
</body>
</html>
