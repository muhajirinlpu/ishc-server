<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 17/10/2017
 * Time: 20:26
 */

namespace Modules\Guest\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Jobs\UpdateSetting;
use App\Models\Device;
use Illuminate\Http\Request;
use Modules\Guest\Models\Code;
use Modules\Guest\Models\Message;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class GuestHandlerController extends Controller
{
    public function index(Device $device)
    {
        $this->setPageTitle('Penerima tamu');
        $this->setData('device', $device);
        $this->setData('inbox', $device->message()->orderBy('created_at', 'desc')->get());

        if ($device->setting()->where('key', 'guest')->count() > 0) {
            $this->setData('guest', $device->setting()->where('key', 'guest')->first());
        }

        if ($device->code()->count() > 0) {
            $this->setData('code', $device->code()->latest()->first());
        }

        return $this->view('guest::index');
    }

    public function codeGenerator(Device $device)
    {
        activity()->performedOn($device)->causedBy(auth()->user())
            ->log('kode penerima tamu telah diganti. harap print ulang kode');

        $code = new Code();

        $code->fill(['device_id' => $device->getAttribute('id')]);

        if(!$code->save())
            return redirect()->back()->withErrors('Error (500)');

        return redirect()->route('app.guest', ['device' => $device->getAttribute('hash')])
            ->with('success', '');
    }

    public function update(Request $request, Device $device)
    {
        $status = 0;

        if ($request->has('status'))
            $status = 1;

        $update = $this->dispatch(new UpdateSetting($device, 'guest', $status));

        if (!$update->success())
            return redirect()->back()->withInput();

        return redirect()->route('app.guest', ['device' => $device->getAttribute('hash')])
            ->with('success', '');
    }

    public function print(Device $device)
    {
        $pdf = app('dompdf.wrapper');
        $code = $device->code()->latest()->first();
        $pdf->loadView('guest::partials.print', [
                'encode' => base64_encode(QrCode::format('png')->size(600)->generate(route('message.show', [$code->code])))
            ]);

        return $pdf->stream();
    }

    public function destroy(Device $device, Message $message)
    {
        if(!$message->delete())
            return redirect()->back()->withErrors('Error (500)');

        return redirect()->route('app.guest', ['device' => $device->getAttribute('hash')])
            ->with('success', '');
    }

    public function mark(Device $device, Message $message)
    {
        $message->read = \request('read');
        if(!$message->save())
            return redirect()->back()->withErrors('Error (500)');

        return redirect()->route('app.guest', ['device' => $device->getAttribute('hash')])
            ->with('success', '');
    }

}