<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 17/10/2017
 * Time: 22:04
 */

namespace Modules\Guest\Http\Controllers;


use App\Http\Controllers\Controller;
use Carbon\Carbon;
use FontLib\Table\Type\name;
use Modules\Guest\Models\Code;
use Modules\Guest\Models\Message;

class GuestMessageController extends Controller
{
    public function index()
    {

    }

    public function show(Code $code)
    {
        if ($code->is_expired)
            return view('error', ['message' => 'Kode kadaluarsa']);

        
        if (!optional($code->device->setting()->where('key', 'guest')->first())->value->first())
            return view('error', ['message' => 'Ada orang dirumah']);

        $this->setPageTitle('Kirim pesan');

        $this->setData('code', $code);

        return $this->view('guest::public.message');
    }

    public function store(Code $code)
    {
        $message = new Message();

        $message->fill([
            'sender' => request('sender'),
            'audio_message' => $this->getAudioMessage(),
            'text_message' => request('text_message'),
            'device_id' => $code->device->id
        ]);

        if ($message->save()) {
            activity()->performedOn($code->device)->causedBy($code->device->user)
                ->log('Terdapat pesan baru . cek di page penerima tamu');
            return 200;
        }

        return 500;
    }

    protected function getAudioMessage()
    {
        if (!request()->hasFile('record'))
            return null;

        $name = Carbon::now()->format('dmyhis').'.webm';
        request()->file('record')->storePubliclyAs('public/record', $name);
        return 'public/record/'.$name;
    }

}