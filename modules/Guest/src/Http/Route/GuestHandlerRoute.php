<?php
namespace Modules\Guest\Http\Route;


use App\Core\Menu\Factory;
use App\Core\Routing\SelfBindingRoute;
use App\Models\User;
use Modules\Guest\Http\Controllers\GuestHandlerController;

class GuestHandlerRoute extends SelfBindingRoute
{

    protected $prefix = 'guest/{device}';

    protected $name = 'guest';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix, [
            'as' => $this->name,
            'uses' => $this->uses('index')
        ])->middleware('alert');

        $this->router->post($this->prefix('code-generator'), [
            'as' => $this->name('code-generator'),
            'uses' => $this->uses('codeGenerator')
        ]);

        $this->router->get($this->prefix('code/print'), [
            'as' => $this->name('print'),
            'uses' => $this->uses('print')
        ]);

        $this->router->post($this->prefix('update'), [
            'as' => $this->name('update'),
            'uses' => $this->uses('update')
        ]);

        $this->router->group(['prefix' => $this->prefix('{message}')], function() {
            $this->router->delete('destroy', [
                'as' => $this->name('destroy'),
                'uses' => $this->uses('destroy')
            ]);

            $this->router->post('mark', [
                'as' => $this->name('mark'),
                'uses' => $this->uses('mark')
            ]);
        });

    }

    public function controller()
    {
        return GuestHandlerController::class;
    }

    public function menu(Factory $factory)
    {
        $menu = $factory->get('menu')->add('#', 'Penerima tamu', [
            'icon' => 'zmdi-face',
            'css_class' => ['nav-dropdown'],
            'active_class' => 'active open',
        ]);

        view()->composer('*', function () use ($menu) {
            /*** @var $user User */
            $user = auth()->user();
            if ($user !== null) {
                if ($user->devices()->count() !== 0) {
                    foreach ($user->devices()->get() as $device) {
                        $menu->addChild(route('app.guest', ['device' => $device->hash]), $device->name);
                    }
                }
            }

            $menu->activeStateResolver(function () {
                return request()->is('application/guest/*');
            });
        });
    }
}