<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 17/10/2017
 * Time: 19:18
 */

namespace Modules\Guest\Http\Route;


use App\Core\Routing\SelfBindingRoute;
use Modules\Guest\Http\Controllers\GuestMessageController;
use Modules\Guest\Models\Code;
use Symfony\Component\HttpKernel\Exception\HttpException;

class GuestMessageRoute extends SelfBindingRoute
{

    protected $prefix = 'message/{code}';

    protected $name = 'message';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->bind('code', function ($value) {
            $data = Code::query()->where('code', $value)->firstOrFail();
            return $data;
        });

        $this->router->get($this->prefix, [
            'as' => $this->name('show'),
            'uses' => $this->uses('show')
        ]);

        $this->router->post($this->prefix('store'), [
            'as' => $this->name('store'),
            'uses' => $this->uses('store')
        ]);
    }

    public function controller()
    {
        return GuestMessageController::class;
    }
}