<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 17/10/2017
 * Time: 20:23
 */

namespace Modules\Guest\Models;


use App\Models\Device;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'guest_message';

    protected $fillable = ['sender', 'audio_message', 'text_message', 'device_id'];

    public function device()
    {
        return $this->belongsTo(Device::class, 'device_id', 'id');
    }
}