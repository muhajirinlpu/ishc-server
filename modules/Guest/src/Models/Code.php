<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 17/10/2017
 * Time: 19:47
 */

namespace Modules\Guest\Models;


use App\Models\Device;
use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Code extends Model
{
    protected $table = 'guest_code_handler';

    protected $fillable = ['device_id', 'code'];

    protected static function boot()
    {
        parent::boot();

        self::creating(function (self $code) {
            $old_code = self::query()->where('device_id', $code->getAttribute('device_id'))
                ->update([
                    'is_expired' => 1
                ]);

            $code->setAttribute('code', Uuid::generate()->string);
        });
    }

    public function device()
    {
        return $this->belongsTo(Device::class, 'device_id', 'id');
    }
}