<?php

namespace Modules\Laundry\Http\Route;

use App\Core\Menu\Factory;
use App\Core\Routing\SelfBindingRoute;
use App\Models\User;
use Modules\Laundry\Http\Controllers\LaundryController;


/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 19/09/17 , 20:01
 */
class LaundryRoute extends SelfBindingRoute
{

    protected $name = 'laundry';

    protected $prefix = 'laundry/{device}';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix, [
            'as' => $this->name,
            'uses' => $this->uses('index')
        ])->middleware('alert');

        $this->router->post($this->prefix('update'), [
            'as' => $this->name('update'),
            'uses' => $this->uses('update')
        ]);
    }

    public function controller()
    {
        return LaundryController::class;
    }

    public function menu(Factory $factory)
    {
        $menu = $factory->get('menu')->add('#', 'Jemuran', [
            'icon' => 'zmdi-cloud-outline-alt',
            'css_class' => ['nav-dropdown'],
            'active_class' => 'active open',
        ]);

        view()->composer('*', function () use ($menu) {
            /*** @var $user User */
            $user = auth()->user();
            if ($user !== null) {
                if ($user->devices()->count() !== 0) {
                    foreach ($user->devices()->get() as $device) {
                        $menu->addChild(route('app.laundry', ['device' => $device->hash]), $device->name);
                    }
                }
            }

            $menu->activeStateResolver(function () {
                return request()->is('application/laundry/*');
            });
        });
    }
}
