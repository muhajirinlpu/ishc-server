<?php

namespace Modules\Laundry\src\Http\Route;

use App\Core\Routing\SelfBindingRoute;
use Modules\Laundry\Http\Controllers\LaundryController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 29/09/17 , 13:06
 */
class LaundryApiRoute extends SelfBindingRoute
{

    protected $prefix = 'laundry/{uuid}';

    protected $name = 'laundry';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->post($this->prefix('turn_off'), [
            'as' => $this->name('turn-off'),
            'uses' => $this->uses('turnOff')
        ]);
    }

    public function controller()
    {
        return LaundryController::class;
    }
}
