<?php

namespace Modules\Laundry\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Jobs\UpdateSetting;
use App\Models\Device;
use App\Models\Setting;
use Illuminate\Http\Request;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 19/09/17 , 20:03
 */
class LaundryController extends Controller
{
    public function index(Device $device)
    {
        $this->setPageTitle('Jemuran panel');

        $status = Setting::query()->where('key', 'laundry');
        if ($status->count() > 0)
            $this->setData('laundry', $status->first()->getAttribute('value'));

        $this->setData('device', $device);
        return $this->view('laundry::index');
    }

    public function update(Request $request, Device $device)
    {
        $this->validate($request, ['schedule' => 'required']);

        $update = $this->dispatch(new UpdateSetting($device, 'laundry', [
            'status' => $request->has('status'),
            'schedule' => $request->input('schedule')
        ]));

        if (!$update->success())
            return redirect()->back()->withInput();

        return redirect()->route('app.laundry', ['device' => $device->getAttribute('hash')])
            ->with('success', '');
    }

    public function turnOff(Request $request, Device $device)
    {
        $laundry = $device->setting()->where('key', 'laundry')
            ->first()->getAttribute('value');

        return $this->dispatch(new UpdateSetting($device, 'laundry', [
            'status' => 0,
            'schedule' => $laundry->get('schedule')
        ]));
    }
}
