<?php

namespace Modules\Laundry\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'laundry');
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'laundry');
        $this->loadMigrationsFrom(__DIR__.'/../../database/migration');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
