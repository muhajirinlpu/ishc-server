@extends('app.container')

@section('content')
<div class="row">
  <form action="{{ route('app.laundry.update', ['device' => $device->hash]) }}"
        method="post" id="status_form">
    {{ csrf_field() }}
    <div class="col-lg-4">
      <div class="card">
        <header class="card-heading card-purple">
          <h2 class="card-title">Status</h2>
        </header>
        <div class="card-body">
          <div class="togglebutton m-b-15 btn-big">
            <label>
              <input type="checkbox" class="toggle-primary"
                     @if(!empty($laundry) && $laundry->has('status') && $laundry->get('status') == true) checked @endif name="status">
            </label>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="card">
        <header class="card-heading card-blue">
          <h2 class="card-title">Jadwal mati</h2>
        </header>
        <div class="card-body">
          <div class="form-group is-empty">
            <div class="input-group">
              <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
              <input name="schedule" class="form-control datepicker md_input_time"
                     type="text" placeholder=""
                     value="@if(!empty($laundry) && $laundry->has('schedule')){{ $laundry->get('schedule') }}@endif">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <button type="reset" class="btn btn-primary btn-flat ">Reset</button>
          <button type="submit" class="btn btn-primary">Apply</button>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection

@push('stylesheet')
  <style>
    .btn-big {
      text-align: center;
      margin-top: 40px;
    }
    .btn-big > * > span {
      transform: scale(2);
    }
  </style>
@endpush
