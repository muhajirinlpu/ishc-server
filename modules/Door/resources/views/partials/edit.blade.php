<div class="modal fade bottom-sheet" id="edit-modal-{{ $item->id }}" tabindex="-1"
     role="dialog" aria-labelledby="edit-modal-{{ $item->id }}">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #E65100">
        <h4 class="modal-title" >Edit {{ $item->name or "(tidak ada nama)" }}</h4>
        <ul class="card-actions icons right-top">
          <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
            <i class="zmdi zmdi-close"></i>
          </a>
          </li>
        </ul>
      </div>
      <div class="modal-body scrollbar">
        <form action="{{ route('app.door.update', [ 'device' => $item->device->hash, 'cardId' => $item->id ]) }}"
              method="post">
          {{ method_field('PUT') }}
          {{ csrf_field() }}
          <div class="form-group">
            <label for="name">Nama</label>
            <input type="text" name="name" id="name" class="form-control" value="{{ $item->name }}">
          </div>
          <div class="togglebutton m-b-15 btn-big">
            <p>Ijin buka pintu</p>
            <label>
              <input type="checkbox" class="toggle-primary"
                     @if($item->allowed) checked @endif name="allowed">
            </label>
          </div>
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Kirim</button>
        </form>
      </div>
    </div>
  </div>
</div>
