<div class="modal fade bottom-sheet" id="delete-modal-{{ $item->id }}" tabindex="-1"
     role="dialog" aria-labelledby="delete-modal-{{ $item->id }}">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #FF1744">
        <h4 class="modal-title" >Yakin ingin menghapus {{ $item->name or "(tidak ada nama)" }}</h4>
        <ul class="card-actions icons right-top">
          <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
            <i class="zmdi zmdi-close"></i>
          </a>
          </li>
        </ul>
      </div>
      <div class="modal-body scrollbar">
        <form action="{{ route('app.door.destroy', [ 'device' => $item->device->hash, 'cardId' => $item->id ]) }}"
              method="post">
          {{ method_field('DELETE') }}
          {{ csrf_field() }}
          <table class="table">
            <tbody>
              <tr>
                <td>Nama</td>
                <td>:</td>
                <td>{{ $item->name or "(tidak ada nama)" }}</td>
              </tr>
              <tr>
                <td>Identitas</td>
                <td>:</td>
                <td>{{ $item->identity }}</td>
              </tr>
              <tr>
                <td>Izin</td>
                <td>:</td>
                <td>{{ $item->allowed ? 'diizinkan' : 'tidak diizinkan' }}</td>
              </tr>
              <tr>
                <td>Di tambahkan pada</td>
                <td>:</td>
                <td>{{ $item->created_at->diffForHumans() }}</td>
              </tr>
            </tbody>
          </table>
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Hapus</button>
        </form>
      </div>
    </div>
  </div>
</div>
