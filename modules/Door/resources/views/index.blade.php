@extends('app.container')

@section('content')
  @each('door::partials.delete', optional($device)->cardId, 'item')
  @each('door::partials.edit', optional($device)->cardId, 'item')
  <div id="content" class="container-fluid">
    <div class="row">
      <div class="col-xs-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-md-8">
                <table class="table">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Identitas</th>
                    <th>Ijin membuka pintu</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  @forelse(optional($device)->cardId as $index => $item)
                    <tr>
                      <td>{{ 1 + $index }}</td>
                      <td>
                        @empty($item->name)
                          (tidak ada nama)
                        @else
                          {{ $item->name }}
                        @endempty
                      </td>
                      <td>{{ $item->identity }}</td>
                      <td>{{ $item->allowed ? 'dizinkan' : 'ditolak' }}</td>
                      <td>
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#edit-modal-{{ $item->id }}">
                          <i class="zmdi zmdi-edit"></i>
                        </a> &nbsp;
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#delete-modal-{{ $item->id }}">
                          <i class="zmdi zmdi-delete"></i>
                        </a>
                      </td>
                    </tr>
                  @empty
                    <tr><td colspan="4">Tidak ada data</td></tr>
                  @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
