<?php

namespace Modules\Door\Http\Route;

use App\Core\Routing\SelfBindingRoute;
use Modules\Door\Http\Controllers\DoorApiController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 08/10/17 , 12:10
 */
class DoorApiRoute extends SelfBindingRoute
{
    protected $prefix = 'door/{uuid}';

    protected $name = 'door';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->post($this->prefix('store'), [
            'uses' => $this->uses('store'),
            'as' => $this->name('store-uid')
        ]);
    }

    public function controller()
    {
        return DoorApiController::class;
    }
}
