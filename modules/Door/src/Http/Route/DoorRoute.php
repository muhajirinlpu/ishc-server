<?php

namespace Modules\Door\Http\Route;

use App\Core\Menu\Factory;
use App\Core\Routing\SelfBindingRoute;
use App\Models\User;
use Modules\Door\Http\Controllers\DoorController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 26/09/17 , 8:42
 */
class DoorRoute extends SelfBindingRoute
{

    protected $prefix = "door/{device}";

    protected $name = "door";

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix, [
            'uses' => $this->uses('index'),
            'as' => $this->name
        ])->middleware('alert');

        $this->router->delete($this->prefix('destroy/{cardId}'), [
            'uses' => $this->uses('destroy'),
            'as' => $this->name('destroy')
        ]);

        $this->router->put($this->prefix('update/{cardId}'), [
            'uses' => $this->uses('update'),
            'as' => $this->name('update')
        ]);
    }

    public function controller()
    {
        return DoorController::class;
    }

    public function menu(Factory $factory)
    {
        $menu = $factory->get('menu')->add('#', 'RFID unlocker', [
            'icon' => 'zmdi-lock-open',
            'css_class' => ['nav-dropdown'],
            'active_class' => 'active open',
        ]);

        view()->composer('*', function () use ($menu) {
            /*** @var $user User */
            $user = auth()->user();
            if ($user !== null) {
                if ($user->devices()->count() !== 0) {
                    foreach ($user->devices()->get() as $device) {
                        $menu->addChild(route('app.door', ['device' => $device->hash]), $device->name);
                    }
                }
            }

            $menu->activeStateResolver(function () {
                return request()->is('application/door/*');
            });
        });
    }
}
