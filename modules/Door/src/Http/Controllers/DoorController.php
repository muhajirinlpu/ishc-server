<?php

namespace Modules\Door\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Device;
use Modules\Door\Jobs\SendDoorSetting;
use Modules\Door\src\Models\CardId;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 26/09/17 , 8:42
 */
class DoorController extends Controller
{

    public function index(Device $device)
    {
        $this->setPageTitle('RFID unlock');
        $this->setData('device', $device);

        return $this->view('door::index');
    }

    public function update(Device $device, CardId $cardId)
    {
        $attributes = $this->validate(request(), [
            'name' => 'required',
        ]);

        $attributes = array_merge($attributes, ['allowed' => request()->has('allowed')]);
        $updated = $cardId->update($attributes);

        $this->dispatch(new SendDoorSetting($device));
        return ($updated)
                    ? redirect()->back()->with('success', 'Diupdate')
                    : redirect()->back()->withErrors('Error (500)');
    }

    public function destroy(Device $device, CardId $cardId)
    {
        $deleted = $cardId->delete();
        $this->dispatch(new SendDoorSetting($device));
        return ($deleted)
                    ? redirect()->back()->with('success', 'Dihapus')
                    : redirect()->back()->withErrors('Error (500)');
    }
}
