<?php

namespace Modules\Door\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Device;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 08/10/17 , 16:15
 */
class DoorApiController extends Controller
{

    public function store(Device $device)
    {
        $data = [
            'status' => 0,
            'data' => ''
        ];

        if (!request()->has('identity')) {
            $data = [
                'status' => 0,
                'data' => "Field ('identity') is required"
            ];
        }

        $cards = $device->cardId()->get()->keyBy('identity');
        if (!$cards->has(request()->input('identity'))) {
            $card = $device->cardId()->create(request()->only('identity'));
            $data['status'] = 1;
        }

        return $data;
    }
}
