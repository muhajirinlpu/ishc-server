<?php

namespace Modules\Door\src\Models;

use App\Models\Device;
use Illuminate\Database\Eloquent\Model;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 08/10/17 , 15:14
 */
class CardId extends Model
{
    protected $table = 'card_id';

    protected $fillable = ['identity', 'name', 'allowed'];

    protected $visible = ['identity'];

    protected $casts = [
        'created_at' => 'datetime'
    ];

    public function device()
    {
        return $this->belongsTo(Device::class, 'device_id', 'id');
    }
}
