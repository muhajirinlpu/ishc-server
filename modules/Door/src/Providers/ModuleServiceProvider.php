<?php

namespace Modules\Door\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'door');
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'door');
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
