<?php

namespace Modules\Door\Jobs;

use App\Core\Bus\AbstractCommand;
use App\Events\UpdateDataRaspi;
use App\Models\Device;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 08/10/17 , 21:18
 */
class SendDoorSetting extends AbstractCommand
{

    public $device;

    public function __construct(Device $device)
    {
        parent::__construct([]);

        $this->device = $device;
    }

    /**
     * Run the actual command process.
     *
     * @return bool
     */
    public function run()
    {
        $allowed_key = [];

        foreach (optional($this->device)->cardId as $cardId) {
            if ($cardId->allowed == 1) {
                array_push($allowed_key, $cardId->identity);
            }
        }

        event(new UpdateDataRaspi($this->device->getAttribute('uuid'), 'door', [
            'key' => $allowed_key
        ]));
    }
}
