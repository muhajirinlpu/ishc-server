<?php

namespace Modules\Monitor\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Device;
use Modules\Monitor\Jobs\StoreNewSensorData;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 18/09/17 , 10:14
 */
class MonitorController extends Controller
{
    public function store(Device $device)
    {
        $cmd = $this->dispatch(new StoreNewSensorData([], $device));

        return $cmd;
    }

    public function getData(Device $device)
    {
        return response()->json([
            'humidity' => $device->sensors()->where('name', 'humidity')->latest('created_at')->first(),
            'light' => $device->sensors()->where('name', 'light')->latest('created_at')->first(),
            'temperature' => $device->sensors()->where('name', 'temperature')->latest('created_at')->first(),
            'rain' => $device->sensors()->where('name', 'rain')->latest('created_at')->first(),
        ]);
    }
}
