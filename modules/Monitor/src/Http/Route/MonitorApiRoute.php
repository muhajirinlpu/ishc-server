<?php

namespace Modules\Monitor\Http\Route;

use App\Core\Routing\SelfBindingRoute;
use Modules\Monitor\Http\Controllers\MonitorController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 18/09/17 , 10:02
 */
class MonitorApiRoute extends SelfBindingRoute
{

    protected $prefix = "monitor/{uuid}";

    protected $name = "monitor";

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get("monitor/data/{device}", [
            'as' => $this->name('get'),
            'uses' => $this->uses('getData')
        ]);

        $this->router->post($this->prefix('store'), [
            'as' => $this->name('store'),
            'uses' => $this->uses('store'),
        ]);

    }

    public function controller()
    {
        return MonitorController::class;
    }
}
