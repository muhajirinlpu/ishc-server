<?php

namespace Modules\Monitor\Http\Route;

use App\Core\Routing\SelfBindingRoute;
use App\Models\Device;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 19/09/17 , 18:53
 */
class Binder extends SelfBindingRoute
{

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->bind('uuid', function ($value) {
            return Device::query()->where('uuid', $value)->firstOrFail();
        });
    }
}
