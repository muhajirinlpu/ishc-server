<?php

namespace Modules\Monitor\Models;


use App\Models\Device;
use Illuminate\Database\Eloquent\Model;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 18/09/17 , 19:11
 */
class Sensor extends Model
{
    protected $table = 'sensors';

    protected $fillable = [
        'device_id',
        'name',
        'value',
    ];

    public function device()
    {
        return $this->belongsTo(Device::class, 'device_id', 'id');
    }
}
