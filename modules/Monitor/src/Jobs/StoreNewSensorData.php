<?php

namespace Modules\Monitor\Jobs;

use App\Core\Bus\AbstractCommand;
use App\Events\SendMessageUser;
use App\Models\Device;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 18/09/17 , 18:51
 */
class StoreNewSensorData extends AbstractCommand
{

    protected $rules = [
        'name' => 'required',
        'value' => 'required'
    ];

    public $device;

    public function __construct(array $inputs = [], Device $device)
    {
        $this->device = $device;

        parent::__construct(array_merge($inputs, [
            'status' => self::FAILED,
            'device' => $this->device
        ]));
    }

    /**
     * Run the actual command process.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function run()
    {
        $this->registerCallback(self::SUCCESS, function () {
            $this->items['status'] = self::SUCCESS;
            broadcast(new SendMessageUser($this->device,"Need update"));
        });

        return $this->device->sensors()->create(
            $this->request->only(array_keys($this->rules))
        );
    }
}
