@extends('app.container')

@section('content')
  <form action="{{ route('app.lamp.update', ['device' => $device->hash]) }}" method="post">
    {{ csrf_field() }}
    <div class="content-body m-t-30">
      <div class="panel-group expansion" id="popout" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#popout" href="#popoutOne" aria-expanded="true" aria-controls="popoutOne">
                Teras
              </a>
            </h4>
          </div>
          <div id="popoutOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="popoutOne">
            <div class="panel-body">
              @include('lamp::partials.form', [
                'content' => 'terrace'
              ])
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#popout" href="#popoutTwo" aria-expanded="false" aria-controls="popoutTwo">
                Ruang tamu
              </a>
            </h4>
          </div>
          <div id="popoutTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="popoutTwo">
            <div class="panel-body">
              @include('lamp::partials.form', [
                'content' => 'living_room'
              ])
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#popout" href="#popoutThree" aria-expanded="false" aria-controls="popoutThree">
                Kamar 1
              </a>
            </h4>
          </div>
          <div id="popoutThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="popoutThree">
            <div class="panel-body">
              @include('lamp::partials.form', [
                'content' => 'room_1'
              ])
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingFour">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#popout" href="#popoutFour" aria-expanded="false" aria-controls="popoutFour">
                Kamar 2
              </a>
            </h4>
          </div>
          <div id="popoutFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="popoutFour">
            <div class="panel-body">
              @include('lamp::partials.form', [
                'content' => 'room_2'
              ])
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingFive">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#popout" href="#popoutFive" aria-expanded="false" aria-controls="popoutFive">
                Kamar mandi
              </a>
            </h4>
          </div>
          <div id="popoutFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="popoutFive">
            <div class="panel-body">
              <div class="col-xs-12 col-sm-12 col-md-4">
                <div class="card">
                  <header class="card-heading">
                    <h2 class="card-title">SWITCH</h2>
                  </header>
                  <div class="card-body">
                    <div class="togglebutton m-b-15 ">
                      <label>
                        <input type="checkbox" class="toggle-primary" name="bathroom"
                          @if(isset($bathroom))
                            @if($bathroom[0]) checked @endif
                          @endif
                        >
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <button type="reset" class="btn btn-primary btn-flat ">Reset</button>
        <button type="submit" class="btn btn-primary">Apply</button>
      </div>
    </div>
  </form>
@endsection
