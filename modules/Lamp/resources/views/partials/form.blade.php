<div class="col-xs-12 col-sm-12 col-md-4">
  <div class="card">
    <header class="card-heading">
      <h2 class="card-title">Nyala Pada</h2>
      <ul class="card-actions icons right-top">
        <li>
          <a href="javascript:void(0)" data-toggle-view="code">
            <i class="zmdi zmdi-code"></i>
          </a>
        </li>
      </ul>
    </header>
    <div class="card-body">
      <div class="form-group is-empty">
        <div class="input-group">
          <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
          <input name="{{ $content }}[on_schedule]" class="form-control datepicker md_input_time"
                 type="text" placeholder="" value="@if(!empty(${$content}) && ${$content}['on_schedule'] !== null) {{ ${$content}['on_schedule'] }} @endif">
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-4">
  <div class="card">
    <header class="card-heading">
      <h2 class="card-title">Mati Pada</h2>
      <ul class="card-actions icons right-top">
        <li>
          <a href="javascript:void(0)" data-toggle-view="code">
            <i class="zmdi zmdi-code"></i>
          </a>
        </li>
      </ul>
    </header>
    <div class="card-body">
      <div class="form-group is-empty">
        <div class="input-group">
          <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
          <input name="{{ $content }}[off_schedule]" class="form-control datepicker md_input_time"
                 type="text" placeholder="" value="@if(!empty(${$content}) && ${$content}['on_schedule'] !== null) {{ ${$content}['off_schedule'] }} @endif">
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-4">
  <div class="card">
    <header class="card-heading">
      <h2 class="card-title">Pengaturan</h2>
    </header>
    <div class="card-body">
      <div class="togglebutton m-b-15">
        <label>
          <input type="checkbox" class="toggle-primary" name="{{ $content }}[automatic]"
          @if(! empty(${$content}) && !empty(${$content}['automatic'])) checked @endif
          > Pembacaan sensor
        </label>
      </div>
    </div>
  </div>
</div>
