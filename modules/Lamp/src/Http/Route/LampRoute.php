<?php

namespace Modules\Lamp\Http\Route;

use App\Models\User;
use App\Core\Menu\Factory;
use App\Core\Routing\SelfBindingRoute;
use Modules\Lamp\Http\Controllers\LampController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 23/08/17 , 18:30
 */
class LampRoute extends SelfBindingRoute
{
    protected $name = 'lamp';

    protected $prefix = 'lamp/{device}';

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix, [
            'as' => $this->name,
            'uses' => $this->uses('index'),
        ])->middleware('alert');

        $this->router->post($this->prefix('update'), [
            'as' => $this->name('update'),
            'uses' => $this->uses('update'),
        ]);
    }

    public function controller()
    {
        return LampController::class;
    }

    public function menu(Factory $factory)
    {
        $menu = $factory->get('menu')->add('#', 'Lampu', [
            'icon' => 'zmdi-lamp',
            'css_class' => ['nav-dropdown'],
            'active_class' => 'active open',
        ]);

        view()->composer('*', function () use ($menu) {
            /*** @var $user User */
            $user = auth()->user();
            if ($user !== null) {
                if ($user->devices()->count() !== 0) {
                    foreach ($user->devices()->get() as $device) {
                        $menu->addChild(route('app.lamp', ['device' => $device->hash]), $device->name);
                    }
                }
            }

            $menu->activeStateResolver(function () {
                return request()->is('application/lamp/*');
            });
        });
    }
}
