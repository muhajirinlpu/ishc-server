<?php

namespace Modules\Lamp\Http\Controllers;

use App\Models\Device;
use App\Http\Controllers\Controller;
use Modules\Lamp\Jobs\UpdateLampProfile;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 23/08/17 , 18:34
 */
class LampController extends Controller
{
    public function index(Device $device)
    {
        $this->setPageTitle('Lampu panel');

        $this->setData('device', $device);

        if ($device->setting()->count() !== 0) {
            foreach ($device->setting()->get() as $setting) {
                $this->setData($setting->key, $setting->value);
            }
        }

        return $this->view('lamp::index');
    }

    public function update(Device $device)
    {
        $command = $this->dispatch(new UpdateLampProfile($device));
        $res = redirect()->back();

        if ($command->success()) {
            return $res->with('success', '');
        }

        return $res->with('errors', '');
    }
}
