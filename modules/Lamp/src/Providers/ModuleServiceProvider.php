<?php

namespace Modules\Lamp\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'lamp');
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'lamp');
        $this->loadMigrationsFrom(__DIR__.'/../../database/migration');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
    }
}
