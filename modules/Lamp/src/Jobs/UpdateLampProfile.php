<?php

namespace Modules\Lamp\Jobs;

use App\Jobs\UpdateSetting;
use App\Models\Device;
use App\Events\UpdateDataRaspi;
use App\Core\Bus\AbstractCommand;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 23/08/17 , 23:37
 */
class UpdateLampProfile extends AbstractCommand
{
    public $device;

    public function __construct(Device $device, array $inputs = [])
    {
        parent::__construct($inputs);

        $this->device = $device;
    }

    /**
     * Run the actual command process.
     *
     * @return bool
     */
    public function run()
    {
        $data = [
            'terrace' => [
                'on_schedule' => $this->request->input('terrace.on_schedule'),
                'off_schedule' => $this->request->input('terrace.off_schedule'),
                'automatic' => $this->request->has('terrace.automatic'),
            ],
            'living_room' => [
                'on_schedule' => $this->request->input('living_room.on_schedule'),
                'off_schedule' => $this->request->input('living_room.off_schedule'),
                'automatic' => $this->request->has('living_room.automatic'),
            ],
            'room_1' => [
                'on_schedule' => $this->request->input('room_1.on_schedule'),
                'off_schedule' => $this->request->input('room_1.off_schedule'),
                'automatic' => $this->request->has('room_1.automatic'),
            ],
            'room_2' => [
                'on_schedule' => $this->request->input('room_2.on_schedule'),
                'off_schedule' => $this->request->input('room_2.off_schedule'),
                'automatic' => $this->request->has('room_2.automatic'),
            ],
            'bathroom' => $this->request->has('bathroom'),
        ];

        foreach ($data as $key => $value)
            dispatch(new UpdateSetting($this->device, $key, $value));

        return true;
    }
}
