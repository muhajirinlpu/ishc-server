<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Routes class
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    */
    'binds' => [
        [
            'provider' => 'web',
            'routes' => [
                \App\Http\Route\BinderRoute::class,
                \Modules\Guest\Http\Route\GuestMessageRoute::class,
            ],
            'child' => [
                [
                    'provider' => 'default',
                    'routes' => [
                        \App\Http\Route\Auth\AuthRoute::class,
                    ],
                ],[
                    'provider' => 'app',
                    'routes' => [
                        \App\Http\Route\Apps\DashboardRoute::class,
                        \App\Http\Route\Apps\AccountRoute::class,
                        \App\Http\Route\Apps\DeviceRoute::class,
                        \Modules\Lamp\Http\Route\LampRoute::class,
                        \Modules\Laundry\Http\Route\LaundryRoute::class,
                        \Modules\Door\Http\Route\DoorRoute::class,
                        \Modules\Guest\Http\Route\GuestHandlerRoute::class,
                    ],
                ],
            ],
        ],[
            'provider' => 'api',
            'routes' => [
                \Modules\Monitor\Http\Route\Binder::class,
                \Modules\Monitor\Http\Route\MonitorApiRoute::class,
                \Modules\Laundry\src\Http\Route\LaundryApiRoute::class,
                \App\Http\Route\Api\ReportRoute::class,
                \Modules\Door\Http\Route\DoorApiRoute::class
            ],
            'child' => [

            ],
        ],
    ],


    /*
    |--------------------------------------------------------------------------
    | Provider for Route
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'providers' => [
        'default' => [],
        'web' => [
            'middleware' => 'web',
        ],
        'api' => [
            'middleware' => 'api',
        ],
        'app' => [
            'as' => 'app.',
            'prefix' => 'application',
            'middleware' => 'auth',
        ],
    ],
];
