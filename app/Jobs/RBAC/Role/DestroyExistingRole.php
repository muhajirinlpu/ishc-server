<?php

namespace App\Jobs\RBAC\Role;

use DCN\RBAC\Models\Role;
use App\Core\Bus\AbstractCommand;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 15/08/17 , 11:41
 */
class DestroyExistingRole extends AbstractCommand
{
    public $role;

    public function __construct(Role $role, array $inputs = [])
    {
        parent::__construct($inputs);

        $this->role = $role;
    }

    /**
     * Run the actual command process.
     *
     * @return bool
     */
    public function run()
    {
        return $this->role->delete();
    }
}
