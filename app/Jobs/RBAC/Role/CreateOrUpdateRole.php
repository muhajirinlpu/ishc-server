<?php

namespace App\Jobs\RBAC\Role;

use DCN\RBAC\Models\Role;
use App\Core\Bus\AbstractCommand;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 15/08/17 , 11:40
 */
class CreateOrUpdateRole extends AbstractCommand
{
    public $role;

    public $rules = [
        'name' => 'required',
        'slug' => 'required|unique:roles',
        'description' => 'required',
        'permission_ids' => 'required|array',
    ];

    public function __construct(Role $role, array $inputs = [])
    {
        parent::__construct($inputs);

        /*
         * fill role with new Role while you want to create new
         */
        $this->role = $role;

        /* Generating slug */
        $this->request->merge([
            'slug' => str_slug($this->request->input('name'), '.'),
        ]);
    }

    /**
     * Run the actual command process.
     *
     * @return bool
     */
    public function run()
    {
        $this->role->fill($this->request->all());

        $this->registerCallback(self::SUCCESS, function () {
            $this->role->permissions()->sync(
                $this->request->input('permission_ids')
            );
        });

        return $this->role->save();
    }
}
