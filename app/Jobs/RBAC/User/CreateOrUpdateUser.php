<?php

namespace App\Jobs\RBAC\User;

use App\Models\User;
use App\Core\Bus\AbstractCommand;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 15/08/17 , 11:42
 */
class CreateOrUpdateUser extends AbstractCommand
{
    public $user;

    protected $rules = [
        'name' => 'required',
        'username' => 'required|unique:users ',
        'email' => 'required|email|unique:users',
        'password' => 'required|confirmed',
        'role' => 'required|array',
    ];

    public function __construct(User $user, array $inputs = [])
    {
        parent::__construct($inputs);

        $this->user = $user;
    }

    /**
     * Run the actual command process.
     *
     * @return bool
     */
    public function run()
    {
        $this->user->fill($this->request->all());

        $this->registerCallback(self::SUCCESS, function () {
            $this->user->roles()->sync($this->request->input('role'));
        });

        return $this->user->save();
    }
}
