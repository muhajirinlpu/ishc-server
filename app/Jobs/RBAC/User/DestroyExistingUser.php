<?php

namespace App\Jobs\RBAC\User;

use App\Models\User;
use App\Core\Bus\AbstractCommand;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 15/08/17 , 11:42
 */
class DestroyExistingUser extends AbstractCommand
{
    public $user;

    public function __construct(User $user, array $inputs = [])
    {
        parent::__construct($inputs);

        $this->user = $user;
    }

    /**
     * Run the actual command process.
     *
     * @return bool
     */
    public function run()
    {
        return $this->user->delete();
    }
}
