<?php

namespace App\Jobs;

use App\Core\Bus\AbstractCommand;
use App\Events\UpdateDataRaspi;
use App\Models\Device;

class UpdateSetting extends AbstractCommand
{
    public $device ;

    public function __construct(Device $device, $key, $value)
    {
        parent::__construct([
            'key' => $key,
            'value' => $value
        ]);

        $this->device = $device;
    }

    /**
     * Run the actual command process.
     *
     * @return bool|\Illuminate\Database\Eloquent\Model
     */
    public function run()
    {
        event(new UpdateDataRaspi($this->device->getAttribute('uuid'),
            $this->get('key'), $this->get('value')));

        return $this->device->setting()->updateOrCreate(['key' => $this->get('key')],
            $this->toArray());
    }
}
