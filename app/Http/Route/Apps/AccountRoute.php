<?php

namespace App\Http\Route\Apps;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Apps\AccountController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 21/10/17 , 22:38
 */
class AccountRoute extends SelfBindingRoute
{
    protected $prefix = 'account';

    protected $name = 'account';
    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get($this->prefix, [
            'uses' => $this->uses('index'),
            'as' => $this->name('index')
        ])->middleware('alert');

        $this->router->put($this->prefix('update'), [
            'uses' => $this->uses('update'),
            'as' => $this->name('update')
        ]);

        $this->router->put($this->prefix('change-password'), [
            'uses' => $this->uses('changePassword'),
            'as' => $this->name('change-password')
        ]);
    }

    public function controller()
    {
        return AccountController::class;
    }
}
