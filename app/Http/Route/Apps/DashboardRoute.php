<?php

namespace App\Http\Route\Apps;

use App\Core\Menu\Factory;
use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Apps\DashboardController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 01/08/17 , 9:33
 */
class DashboardRoute extends SelfBindingRoute
{
    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get('dashboard', [
            'as' => 'dashboard',
            'uses' => $this->uses('index'),
        ]);
    }

    public function controller()
    {
        return DashboardController::class;
    }

    public function menu(Factory $factory)
    {
        $factory->get('menu')->add(route('app.dashboard'), 'Dashboard', [
            'icon' => 'zmdi-view-dashboard',
        ]);
    }
}
