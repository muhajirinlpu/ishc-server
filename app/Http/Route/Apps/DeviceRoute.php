<?php

namespace App\Http\Route\Apps;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Apps\DeviceController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 22/10/17 , 11:20
 */
class DeviceRoute extends SelfBindingRoute
{
    protected $prefix = 'device';

    protected $name = 'device';
    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->resource($this->name, DeviceController::class, [
            'only' => ['store', 'update', 'destroy']
        ]);
    }
}
