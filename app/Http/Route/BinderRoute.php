<?php

namespace App\Http\Route;

use App\Models\Device;
use App\Core\Routing\SelfBindingRoute;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 23/07/17 , 1:46
 */
class BinderRoute extends SelfBindingRoute
{
    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get('/', function () {
            return redirect()->route('auth.login');
        });

        $this->router->bind('device', function ($value) {
            return Device::byHashOrFail($value);
        });

    }
}
