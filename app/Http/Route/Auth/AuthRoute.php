<?php

namespace App\Http\Route\Auth;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\Auth\LoginController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 30/07/17 , 8:09
 */
class AuthRoute extends SelfBindingRoute
{
    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->get('auth/login', [
            'as' => 'auth.login.form',
            'uses' => $this->uses('showLoginForm'),
        ])->middleware('alert');

        $this->router->post('auth/login', [
            'as' => 'auth.login',
            'uses' => $this->uses('login'),
        ]);

        $this->router->post('auth/logout', [
            'as' => 'auth.logout',
            'uses' => $this->uses('logout'),
        ]);
    }

    public function controller()
    {
        return LoginController::class;
    }
}
