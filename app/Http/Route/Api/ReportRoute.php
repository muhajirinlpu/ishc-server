<?php

namespace App\Http\Route\Api;

use App\Core\Routing\SelfBindingRoute;
use App\Http\Controllers\ReportController;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 19/09/17 , 18:50
 */
class ReportRoute extends SelfBindingRoute
{

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    public function register()
    {
        $this->router->post('report/{uuid}/store', [
            'as' => 'report.store',
            'uses' => $this->uses('store')
        ]);

        $this->router->get('get-setting/{uuid}/{key}', [
            'as' => 'get.setting',
            'uses' => $this->uses('getSetting')
        ]);
    }

    public function controller()
    {
        return ReportController::class;
    }
}
