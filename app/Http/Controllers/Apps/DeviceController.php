<?php

namespace App\Http\Controllers\Apps;

use App\Http\Controllers\Controller;
use App\Models\Device;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 22/10/17 , 11:21
 */
class DeviceController extends Controller
{
    public function store()
    {
        $data = $this->validate(request(), [
            'name' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);

        $data['user_id'] = auth()->user()->id;

        if(!Device::query()->create($data))
            return redirect()->back()->withErrors('Error (500)');

        return redirect()->route('app.account.index')->with('success', 'Device ditambahkan');
    }

    public function update(Device $device)
    {
        $data = $this->validate(request(), [
            'name' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);

        if(!$device->update($data))
            return redirect()->back()->withErrors('Error (500)');

        return redirect()->route('app.account.index')->with('success', 'Device diedit');
    }

    public function destroy(Device $device)
    {
        if(!$device->delete())
            return redirect()->back()->withErrors('Error (500)');

        return redirect()->route('app.account.index')->with('success', 'Device diedit');
    }
}
