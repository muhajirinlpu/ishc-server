<?php

namespace App\Http\Controllers\Apps;

use App\Http\Controllers\Controller;
use App\Models\User;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 21/10/17 , 22:39
 */
class AccountController extends Controller
{
    public function index()
    {
        $this->setPageTitle('Pengaturan Akun');

        return $this->view('app.account.index');
    }

    public function update()
    {
        $data = $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'username' => 'required',
        ]);

        /**
         * @var User $user
         */
        $user = auth()->user();

        if(!$user->update($data))
            return redirect()->back()->withErrors('Error (500)');

        return redirect()->route('app.account.index')->with('success', 'Akun diganti');
    }

    public function changePassword()
    {
        $this->validate(request(), [
            'password' => 'required|confirmed'
        ]);

        /**
         * @var User $user
         */
        $user = auth()->user();

        if(!$user->update([
            'password' => request('password')
        ]))
            return redirect()->back()->withErrors('Error (500)');

        auth()->logout();
        return redirect()->route('auth.login.form')->with('success', 'Password Diganti  ');
    }
}
