<?php

namespace App\Http\Controllers\Apps;

use App\Http\Controllers\Controller;
use App\Models\User;

class DashboardController extends Controller
{
    public function index()
    {
        $this->setPageTitle('Dashboard');

        return $this->view('app.dashboard');
    }
}
?>