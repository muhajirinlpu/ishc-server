<?php

namespace App\Http\Controllers;

use App\Models\Device;
use Illuminate\Http\Request;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 19/09/17 , 19:11
 */
class ReportController extends Controller
{
    public function store(Request $request, Device $device)
    {
        // TODO : change this if many to many users -> devices implemented
        return activity()->performedOn($device)->causedBy($device->user)
            ->log($request->input('report', ''));
    }

    public function getSetting(Request $request, Device $device, $key)
    {
        return $device->setting()->where('key', $key)->firstOrFail()->value;
    }
}
