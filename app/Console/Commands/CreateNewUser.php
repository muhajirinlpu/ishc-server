<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class CreateNewUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new user by administrator';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->ask("Masukkan Nama : ");
        $username = $this->ask("Masukkan Username : ");
        $email = $this->ask("Masukkan Email : ");
        $password = $this->secret("Masukkan Password : ");

        if ($this->confirm('Lanjutkan ?')) {
            if (User::query()->create([
                'name' => $name,
                'username' => $username,
                'email' => $email,
                'password' => $password
            ])) {
                $this->info("User Created");
            } else {
                $this->error("Something went wrong");
            }
        } else {
            $this->error("Something went wrong");
        }
    }
}
