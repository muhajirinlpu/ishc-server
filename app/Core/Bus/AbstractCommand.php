<?php

namespace App\Core\Bus;

use Closure;
use BadMethodCallException;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * Command abstract.
 *
 * @author      veelasky <veelasky@gmail.com>
 */
abstract class AbstractCommand extends Collection
{
    use ValidatesRequests;

    const SUCCESS = 'success';

    const FAILED = 'failed';

    /** @var $request \Illuminate\Http\Request */
    public $request;

    public $inputs;

    protected $rules = [];

    /**
     * Command Status.
     *
     * @var string
     */
    protected $status = 'idle';

    /**
     * List of all registered callbacks.
     *
     * @var array
     */
    protected $callbacks = [];

    /**
     * Original Returning Object From Command.
     *
     * @var array
     */
    private $originalObject = null;

    /**
     * AbstractCommand constructor.
     *
     * @param array $inputs
     */
    public function __construct($inputs = [])
    {
        $this->inputs = $inputs;
        $this->request = app('request');
        $this->request->merge($inputs);

        parent::__construct($inputs);
    }

    /**
     * Handle incoming job.
     *
     * @return $this
     */
    public function handle()
    {
        // Determine if this command has a boot method, which is convenient when developer
        // needs to modify any information on this command before actually running it.
        if (method_exists($this, 'boot')) {
            $this->boot();
        }

        $this->fireCallbacks('running');

        $this->validate($this->request, $this->rules);

        $status = $this->run();
        $this->setOriginalObject($status);
        $this->status = ($status) ? self::SUCCESS : self::FAILED;

        // base on the outcome of the run method, let run additional callbacks.
        $this->fireCallbacks($this->status);

        return $this;
    }

    /**
     * Get value of request only on array keys validation rule.
     *
     * @return array
     */
    public function getRequestFromValidation()
    {
        return $this->request->only(array_keys($this->rules));
    }

    /**
     * Fire all callbacks registered on the callbacks array.
     *
     * @param string $event
     *
     * @return void
     */
    protected function fireCallbacks($event)
    {
        if (array_key_exists($event, $this->callbacks)) {
            foreach ($this->callbacks[$event] as $callback) {
                call_user_func($callback, $this);
            }
        }
    }

    /**
     * Run the actual command process.
     *
     * @return bool
     */
    abstract public function run();

    /**
     * Immediately calling abort callbacks.
     *
     * @return void
     */
    public function abort()
    {
        $this->fireCallbacks('abort');
    }

    /**
     * Set Command Status.
     *
     * @param $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get Original Returning Object From Command.
     * @return array
     * @internal param $status
     */
    public function getOriginalObject()
    {
        return $this->originalObject;
    }

    /**
     * Set Original Returning Object From Command.
     *
     * @param $originalObject
     * @internal param $original
     * @internal param $status
     */
    public function setOriginalObject($originalObject)
    {
        $this->originalObject = $originalObject;
    }

    /**
     * Register Callback to the callbacks array.
     *
     * @param          $event
     * @param \Closure $callback
     *
     * @return $this
     */
    public function registerCallback($event, Closure $callback)
    {
        $this->callbacks[$event][] = $callback;

        return $this;
    }

    /**
     * Determine if job is succeeded.
     *
     * @return bool
     */
    public function success()
    {
        return ($this->status === self::SUCCESS);
    }

    /*
     * Determine if job is Failed
     *
     * @return bool
     */
    public function failed()
    {
        return ($this->status === self::FAILED);
    }

    /**
     * Make dynamic call to the command.
     *
     * @param $name
     * @param $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        if (Str::startsWith($name, 'on')) {
            $event = substr($name, 2);

            return $this->registerCallback(Str::lower($event), $arguments[0]);
        }

        throw new BadMethodCallException("Invalid Method `$name`");
    }
}
