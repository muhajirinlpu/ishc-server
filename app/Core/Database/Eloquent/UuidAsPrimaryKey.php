<?php

namespace App\Core\Database\Eloquent;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

/**
 * UUID as primary key in eloquent model.
 *
 * @author      veelasky <veelasky@gmail.com>
 */
trait UuidAsPrimaryKey
{
    public static function bootUuidAsPrimaryKey()
    {
        self::creating(function (Model $model) {
            $model->{$model->getKeyName()} = Uuid::generate()->string;
        });
    }
}
