<?php

namespace App\Core\Contracts\HashId;

use Illuminate\Contracts\Config\Repository as ConfigInterface;

/**
 * class.id HashId Repository Contract.
 *
 * @author          veelasky <veelasky@gmail.com>
 */
interface Repository extends ConfigInterface
{
}
