<?php

namespace App\Core\Routing;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

/**
 * Route Group Service Provider.
 *
 * @author      veelasky <veelasky@gmail.com>
 * @development muhajirin <muhajirinlpu@gmail.com>
 */
class RouteServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerRoutes(config('routes.binds'));
    }

    private function registerRoutes($binds = [])
    {
        foreach ($binds as $item) {
            app('router')->group(config('routes.providers')[$item['provider']], function () use ($item) {
                foreach ($item['routes'] as $route) {
                    $route::bind();
                }

                if (isset($item['child'])) {
                    $this->registerRoutes($item['child']);
                }
            });
        }
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        //
    }
}
