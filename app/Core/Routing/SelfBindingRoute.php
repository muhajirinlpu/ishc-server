<?php

namespace App\Core\Routing;

use RuntimeException;
use App\Core\Contracts\Routing\Binder;
use App\Core\Menu\Factory as MenuFactory;
use Illuminate\Contracts\Routing\Registrar;

/**
 * Self Binding Route.
 *
 * @author      veelasky <veelasky@gmail.com>
 */
abstract class SelfBindingRoute implements Binder
{
    /**
     * Route path prefix.
     *
     * @var string
     */
    protected $prefix = '/';

    /**
     * Registered route name.
     *
     * @var string
     */
    protected $name;

    /**
     * Router Registrar.
     *
     * @var \Illuminate\Routing\Router $router
     */
    protected $router;

    /**
     * Create new route register.
     *
     * @var \Illuminate\Contracts\Routing\Registrar
     */
    public function __construct(Registrar $router)
    {
        $this->router = $router;
    }

    /**
     * Bind and register the current route.
     *
     * @return void
     */
    public static function bind()
    {
        $route = new static(app(Registrar::class));

        $route->register();

        $route->afterRegister();
    }

    /**
     * Perform after register callback.
     *
     * @return void
     */
    public function afterRegister()
    {
        if (method_exists($this, 'menu') and app('core.menu') instanceof MenuFactory) {
            $this->menu(app('core.menu'));
        }
    }

    /**
     * Register routes handled by this class.
     *
     * @return void
     */
    abstract public function register();

    /**
     * Use controller method.
     *
     * @param $method string
     * @param $controller string
     *
     * @return string
     */
    public function uses($method, $controller = null)
    {
        if (! method_exists($this, 'controller') and empty($controller)) {
            throw new RuntimeException('Controller is not defined.');
        }

        $controller = empty($controller) ? $this->controller() : $controller;

        return $controller.'@'.$method;
    }

    /**
     * Get route prefix.
     *
     * @param string $path
     *
     * @return string
     */
    public function prefix($path = '/')
    {
        $qualifiedPath = $this->prefix.'/'.ltrim($path, '/');

        return str_replace('//', '/', $qualifiedPath);
    }

    /**
     * Get route name.
     *
     * @param null|string $suffix
     *
     * @return string
     */
    public function name($suffix = null)
    {
        if (empty($suffix)) {
            return $this->name;
        }

        return $this->name.'.'.$suffix;
    }
}
