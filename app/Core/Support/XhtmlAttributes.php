<?php

namespace app\Core\Support;

trait XhtmlAttributes
{
    /**
     * Send ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendXhtmlFailedResponse()
    {
        return response()->json([
            'message' => 'There\'s something wrong with your request',
        ], 500);
    }

    /**
     * Send success ajax response.
     *
     * @param array $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendXhtmlSuccessResponse($data = [])
    {
        return response()->json($data, 200);
    }
}
