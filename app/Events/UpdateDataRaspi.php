<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

/**
 * @property bool hard
 * @property string uuid
 */
class UpdateDataRaspi implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $key = null;
    public $value = null;

    protected $uuid;

    /**
     * Create a new event instance.
     *
     * @param string $uuid
     * @param string $key
     * @param array $value
     */
    public function __construct(string $uuid, string $key, $value)
    {
        $this->key = (string) $key;
        $this->value = $value;

        $this->uuid = $uuid;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('raspberry-pi-'.$this->uuid);
    }
}
