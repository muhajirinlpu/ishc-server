<?php

namespace App\Models;

use Modules\Door\src\Models\CardId;
use Modules\Guest\Models\Code;
use Modules\Guest\Models\Message;
use Modules\Monitor\Models\Sensor;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use App\Core\HashId\Eloquent\HashableId;

class Device extends Model
{
    use HashableId;

    protected $table = 'devices';

    protected $fillable = ['user_id', 'uuid', 'name', 'longitude', 'latitude'];

    protected static function boot()
    {
        parent::boot();

        self::creating(function (Model $model) {
            $model->setAttribute('uuid', Uuid::generate()->string);
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function setting()
    {
        return $this->hasMany(Setting::class, 'device_id', 'id');
    }

    public function sensors()
    {
        return $this->hasMany(Sensor::class, 'device_id', 'id');
    }

    public function cardId()
    {
        return $this->hasMany(CardId::class, 'device_id', 'id');
    }

    public function message()
    {
        return $this->hasMany(Message::class, 'device_id', 'id');
    }

    public function code()
    {
        return $this->hasMany(Code::class, 'device_id', 'id');
    }
}
