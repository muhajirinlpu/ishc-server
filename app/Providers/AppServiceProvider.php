<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
use Spatie\Activitylog\Models\Activity;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //if ($this->app->environment('local', 'testing')) {
            // $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
        //}

        $this->app['core.menu']->make('menu', ['view' => 'partials.menu.left-sidebar']);
    }
}
