var gulp = require('gulp');
var gulpFilter = require('gulp-filter');
var fs = require('fs');
$ = require('gulp-load-plugins')();
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-clean-css');
var gulpBowerFiles = require('main-bower-files');
var sass = require('gulp-sass');
var del = require('del');


gulp.task('vendor', function(){
  var jsFilter = $.filter('**/*.js', {restore: true});
  var cssFilter = $.filter('**/*.css', {restore: true});
  var fontsFilter = $.filter('**/*.{ttf,woff,woff2,eof,svg}', {restore: true});
  var imgFilter = $.filter('**/*.{png,jpg}', {restore: true});
  var vendorSrc = JSON.parse(fs.readFileSync('vendor.json', 'utf8'));

      return gulp.src(vendorSrc, {
          base: 'bower_components'
      })
      .pipe(jsFilter)
      .pipe(concat('vendor.js'))
      .pipe(gulp.dest('public/js'))
      .pipe(rename('vendor.bundle.js'))
      .pipe(uglify())
      .pipe(gulp.dest('public/js'))
      .pipe(jsFilter.restore)
      .pipe(cssFilter)
      .pipe(concat('vendor.css'))
      .pipe(gulp.dest('public/css'))
      .pipe(rename('vendor.bundle.css'))
      .pipe(minifyCSS())
      .pipe(gulp.dest('public/css'))
      .pipe(cssFilter.restore)
      .pipe(imgFilter)
      .pipe($.flatten())
      .pipe(gulp.dest('public/img'))
      .pipe(imgFilter.restore)
      .pipe(fontsFilter)
      .pipe($.flatten())
      .pipe(gulp.dest('public/fonts'))
      .on("error", errorAlert);
});


gulp.task('sass', function () {
  return gulp.src('resources/assets/template/sass/app.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(sass({includePaths: ['resources/assets/template/sass']}))
    .pipe(gulp.dest('public/css'))
    .pipe(rename('app.bundle.css'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('public/css'));
});

//Error handeling
function errorAlert(err) {
	console.log(err.toString());
	this.emit("end");
}

gulp.task('default', ['vendor', 'sass']);
