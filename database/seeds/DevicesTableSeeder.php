<?php

use Illuminate\Database\Seeder;

class DevicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\Models\User::find(1);

        $user->devices()->save(new \App\Models\Device([
            'name' => 'Rumah Kayu',
        ]));
    }
}
