<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\Models\User([
            'name' => 'administrator',
            'username' => 'admin',
            'email' => 'admin@mail.com',
            'password' => 'password',
        ]);

        $user->save();
    }
}
