const app = require('http').createServer(handler);
const io = require('socket.io')(app);

const Redis = require('ioredis');
const redis = new Redis();

app.listen(6001, function() {
    console.log('Server running on 127.0.0.1:6001');
});

function handler(req, res) {
    res.writeHead(200);
    res.end('');
}

io.on('connection', function(socket) {
    console.log('New Connection!');
});

io.on('disconnect',function(socket){
    console.log('Connection Dropped!');
});

redis.psubscribe('*', function(err, count) {
    console.log(count);
});

redis.on('pmessage', function(subscribed, channel, message) {
    console.log(channel + " : " + message);
    message = JSON.parse(message);
    io.emit(channel, message.data);
});

