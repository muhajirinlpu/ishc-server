let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js(['resources/assets/template/js/material/material.js', 'resources/assets/template/js/material/ripples.js',
		 'resources/assets/template/js/initApp.js', 'resources/assets/template/js/app.js'], 'public/js/app.bundle.js')
    .react('modules/Guest/resources/assets/recorder.jsx', 'public/js/components/')
    .sass('resources/assets/sass/app.scss', 'public/css');
