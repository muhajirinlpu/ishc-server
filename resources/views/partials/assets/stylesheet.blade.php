<link rel="stylesheet" href="{{ asset('css/vendor.bundle.css') }}">
<link rel="stylesheet" href="{{ asset('css/app.bundle.css') }}">
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
@stack('stylesheet')
