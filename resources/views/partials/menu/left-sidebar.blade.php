<aside id="app_sidebar-left">
  <div id="logo_wrapper">
    <ul>
      <li class="logo-icon">
        <a href="/">
          <div class="logo">
            <img src="{{ asset('img/logo.png') }}" alt="Logo">
          </div>
          <h1 class="brand-text">{{ config('app.name') }}</h1>
        </a>
      </li>
      <li class="menu-icon">
        <a href="javascript:void(0)" role="button" data-toggle-state="app_sidebar-menu-collapsed" data-key="leftSideBar">
          <i class="mdi mdi-backburger"></i>
        </a>
      </li>
    </ul>
  </div>
  <nav id="app_main-menu-wrapper" class="scrollbar">
    <div class="sidebar-inner sidebar-push">
      <ul class="nav nav-pills nav-stacked">
        <li class="sidebar-header">NAVIGATION</li>
        @foreach ($menu->all() as $item)
          @if ($item->renderable())
            <li class="{{ $item->cssClasses() }} {{ $item->active() ? $item->active_class : '' }}">
              <a href="{{ $item->uri }}">
                <i class="zmdi {!! $item->icon !!}"></i>
                {{ $item->text }}
                @if ($item->hasBadge())
                  <span class="nav-label">
                    <b class="label label-sm primary">{!! $item->getBadgeContent() !!}</b>
                  </span>
                @endif
              </a>
              @if($item->hasChildren())
                <ul class="nav-sub">
                  @foreach ($item->children as $item)
                    @if ($item->renderable())
                      <li class="{{ $item->cssClasses() }} {{ $item->active() ? $item->active_class : '' }}">
                        <a href="{{ $item->uri }}">
                          <span class="zmdi {!! $item->icon !!}"></span>
                          {{ $item->text }}
                        </a>
                      </li>
                    @endif
                  @endforeach
                </ul>
              @endif
            </li>
          @endif
        @endforeach
      </ul>
    </div>
  </nav>
</aside>
