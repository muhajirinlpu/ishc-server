@extends('app.container')

@section('content')
  <div class="content-body m-t-30">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      @foreach(auth()->user()->devices as $device)
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading-{{ $device->hash }}">
            <h4 class="panel-title">
              <a role="button" data-toggle="collapse" data-parent="#accordion"
                 href="#collapse-{{ $device->hash }}" aria-expanded="true" aria-controls="collapse-{{ $device->hash }}">
                {{ $device->name }}
              </a>
            </h4>
          </div>
          <div id="collapse-{{ $device->hash }}" class="panel-collapse collapse in" role="tabpanel"
               aria-labelledby="heading-{{ $device->hash }}">
            <div class="panel-body">
              <div class="row">
                <div class="col-lg-4">
                  <div class="card">
                    <header class="card-heading card-image app_primary_bg">
                      <img src="{{ asset('img/unnamed-11.png') }}" alt="" class="mCS_img_loaded">
                      <h2 class="card-title left-bottom overlay text-white">Kelembapan</h2>
                    </header>
                    <div class="card-body" style="position: relative; text-align: center">
                      <div class="zmdi zmdi-cloud-outline" style="font-size: 10em"></div>
                      <h4 style="position: absolute;left: 0; right: 0;top: 45%;" id="humidity-{{ $device->hash }}">
                        NULL
                      </h4>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="card">
                    <header class="card-heading card-image app_primary_bg">
                      <img src="{{ asset('img/unnamed-18.png') }}" alt="" class="mCS_img_loaded">
                      <h2 class="card-title left-bottom overlay text-white">Suhu</h2>
                    </header>
                    <div class="card-body" style="position: relative; text-align: center">
                      <div class="zmdi zmdi-cloud-outline" style="font-size: 10em"></div>
                      <h4 style="position: absolute;left: 0; right: 0;top: 45%;" id="temperature-{{ $device->hash }}">
                        NULL
                      </h4>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="card">
                    <header class="card-heading card-image app_primary_bg">
                      <img src="{{ asset('img/header-md-09.jpg') }}" alt="" class="mCS_img_loaded">
                      <h2 class="card-title left-bottom overlay text-white">Cahaya</h2>
                    </header>
                    <div class="card-body" style="position: relative; text-align: center">
                      <div class="zmdi zmdi-cloud-outline" style="font-size: 10em"></div>
                      <h4 style="position: absolute;left: 0; right: 0;top: 45%;" id="light-{{ $device->hash }}">
                        NULL
                      </h4>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="card">
                    <header class="card-heading card-image app_primary_bg">
                      <img src="{{ asset('img/unnamed-23.png') }}" alt="" class="mCS_img_loaded">
                      <h2 class="card-title left-bottom overlay text-white">Hujan</h2>
                    </header>
                    <div class="card-body" style="position: relative; text-align: center">
                      <div class="zmdi zmdi-cloud-outline" style="font-size: 10em"></div>
                      <h4 style="position: absolute;left: 0; right: 0;top: 45%;" id="rain-{{ $device->hash }}">
                        NULL
                      </h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
@endsection

@push('javascript')
  <script src="{{ asset('js/socket.io.min.js') }}"></script>
  <script>
    function getData() {
      @foreach(auth()->user()->devices as $device)
      axios.get('/monitor/data/{{ $device->hash }}').then(function (data) {
        $("#humidity-{{ $device->hash }}").text(parseInt(data.data.humidity.value) + " %");
        $("#rain-{{ $device->hash }}").text(data.data.rain.value);
        $("#temperature-{{ $device->hash }}").text(parseInt(data.data.temperature.value) + "°");
        $("#light-{{ $device->hash }}").text(data.data.light.value);
      });
      @endforeach
    }
    var socket = io("{{ request()->getHost() }}" + ":6001");

    socket.on('connect', function() {
      console.log('Device connected with ID : ' + socket.id);
      getData();
    });

    socket.on('private-user', function(data) {
      console.log(data.message);
      getData();
    });
  </script>
@endpush
