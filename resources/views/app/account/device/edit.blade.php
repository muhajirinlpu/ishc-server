<div class="modal fade bottom-sheet" id="edit-modal-{{ $item->id }}" tabindex="-1"
     role="dialog" aria-labelledby="edit-modal-{{ $item->id }}">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #E65100">
        <h4 class="modal-title" >Edit {{ $item->name or "(tidak ada nama)" }}</h4>
        <ul class="card-actions icons right-top">
          <li>
            <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
              <i class="zmdi zmdi-close"></i>
            </a>
          </li>
        </ul>
      </div>
      <div class="modal-body scrollbar">
        <form action="{{ route('app.device.update', ['device' => $item->hash]) }}"
              method="post">
          {{ method_field('PUT') }}
          {{ csrf_field() }}
          <div class="form-group">
            <label for="name">Nama</label>
            <input type="text" name="name" id="name" class="form-control" value="{{ $item->name }}">
          </div>
          <div class="form-group">
            <label for="name">Latitude</label>
            <input type="text" name="latitude" id="name" class="form-control" value="{{ $item->latitude }}">
          </div>
          <div class="form-group">
            <label for="name">Longitude</label>
            <input type="text" name="longitude" id="name" class="form-control" value="{{ $item->longitude }}">
          </div>
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Kirim</button>
        </form>
      </div>
    </div>
  </div>
</div>
