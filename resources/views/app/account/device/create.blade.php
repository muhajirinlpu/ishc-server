<div class="modal fade bottom-sheet" id="create-modal" tabindex="-1"
     role="dialog" aria-labelledby="create-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" >
        <h4 class="modal-title" >Tambah Rumah</h4>
        <ul class="card-actions icons right-top">
          <li>
            <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
              <i class="zmdi zmdi-close"></i>
            </a>
          </li>
        </ul>
      </div>
      <div class="modal-body scrollbar">
        <form action="{{ route('app.device.store') }}"
              method="post">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="name">Nama</label>
            <input type="text" name="name" id="name" class="form-control">
          </div>
          <div class="form-group">
            <label for="name">Latitude</label>
            <input type="text" name="latitude" id="name" class="form-control">
          </div>
          <div class="form-group">
            <label for="name">Longitude</label>
            <input type="text" name="longitude" id="name" class="form-control" >
          </div>
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Kirim</button>
        </form>
      </div>
    </div>
  </div>
</div>
