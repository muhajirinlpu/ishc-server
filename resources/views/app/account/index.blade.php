@extends('app.container')

@section('content')
  @include('app.account.device.create')
  @each('app.account.device.edit', auth()->user()->devices, 'item')
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <header class="card-heading">
        <h2 class="card-title">Pengaturan akun</h2>
      </header>
      <form class="form-horizontal" method="post" action="{{ route('app.account.update') }}">
        <div class="card-body">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
          <div class="form-group is-empty">
            <label for="name" class="col-md-2 control-label">Nama</label>
            <div class="col-md-10">
              <input type="text" class="form-control" id="name" placeholder="Nama" name="name" value="{{ auth()->user()->name }}">
            </div>
          </div>
          <div class="form-group is-empty">
            <label for="username" class="col-md-2 control-label">Username</label>
            <div class="col-md-10">
              <input type="text" class="form-control" id="username" placeholder="Username" name="username" value="{{ auth()->user()->username }}">
            </div>
          </div>
          <div class="form-group is-empty">
            <label for="inputEmail" class="col-md-2 control-label">Email</label>
            <div class="col-md-10">
              <input type="email" class="form-control" id="inputEmail" placeholder="Email" name="email" value="{{ auth()->user()->email }}">
            </div>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-danger">Submit</button>
        </div>
      </form>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card">
      <header class="card-heading">
        <h2 class="card-title">Ganti password</h2>
      </header>
      <form class="form-horizontal" method="post" action="{{ route('app.account.change-password') }}">
        <div class="card-body">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
          <div class="form-group is-empty">
            <label for="inputPassword" class="col-md-2 control-label">Password Baru</label>
            <div class="col-md-10">
              <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password">
            </div>
          </div>
          <div class="form-group is-empty">
            <label for="inputPasswordConfirmation" class="col-md-2 control-label">Password Konfirmasi</label>
            <div class="col-md-10">
              <input type="password" class="form-control" id="inputPasswordConfirmation" placeholder="Password" name="password_confirmation">
            </div>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-danger">Submit</button>
        </div>
      </form>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card">
      <header class="card-heading">
        <h2 class="card-title">Rumah anda</h2>
        <p>* UUID digunakan untuk di device (Rumah) . silahkan untuk teknisi memasukkan uuid saat pemasangan</p>
        <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#create-modal">tambah device</button>
      </header>
      <div class="card-body">
        <table class="table">
          <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>UUID</th>
            <th>Longitude</th>
            <th>Latitude</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          @foreach(auth()->user()->devices as $key => $device)
            <tr>
              <td>{{ $key + 1 }}</td>
              <td>{{ $device->name }}</td>
              <td>{{ $device->uuid }}</td>
              <td>{{ $device->longitude }}</td>
              <td>{{ $device->latitude }}</td>
              <td>
                <a href="javascript:void(0)" data-toggle="modal" data-target="#edit-modal-{{ $device->id }}">
                  <i class="zmdi zmdi-edit"></i>
                </a>
                <form action="{{ route('app.device.destroy', ['device' => $device->hash]) }}" method="post" class="hidden" id="delete-form-{{ $device->hash }}">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
                </form>
                <a href="javascript:void(0)" class="swal-warn" data-title="Yakin ingin menghapus pesan dari {{ $device->name }}"
                   data-subtitle="Data tidak akan bisa kembali :("
                   data-target="#delete-form-{{ $device->hash }}">
                  <i class="zmdi zmdi-delete"></i>
                </a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@push('javascript')
  <script>
    $('.swal-warn').bind('click', function(e) {
      e.preventDefault();
      const target = $(this).data('target');
      const title = $(this).data('title');
      const subtitle = $(this).data('subtitle');
      swal({
        title   : title,
        text    : subtitle,
        type    : "warning",
        showCancelButton    : true,
        confirmButtonColor  : "#DD6B55",
        confirmButtonText   : "Oke"
      }).then(function() {
        $(target).submit();
        console.log(target)
      }, function (dismiss) {
        if (dismiss === 'cancel') {
          swal(
            'Cancelled',
            'Your imaginary file is safe :)',
            'error'
          )
        }
      })
    });
  </script>
@endpush
